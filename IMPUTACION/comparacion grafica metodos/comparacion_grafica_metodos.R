# COMPARACION GRAFICA METODOS

library(ggplot2)

# VPN, especificidad, VPN, VPN y %_aciertos de cada metodo sobre la media de todas las muestras ----
load("./IMPUTACION/comparacion grafica metodos/datos_graficas.RData")

porc_aciertos_MetaIntegrator <- mean(c(98.13,100,98.28,100,98.80,97.50,100,100,100))
porc_aciertos_massiR <- mean(c(95.33,92.54,100,98.28,100,98.80,97.50,100,100,100))
sensibilidad_MetaIntegrator <- mean(c(98.55,100,92.31,100,97.92,95,100,100,100))
sensibilidad_massiR <- mean(c(94.20,91,100,92.31,100,95.83,95,100,100,100))
especificidad_MetaIntegrator <- mean(c(97.37,100,100,100,99.15,100,100,100,100))
especificidad_massiR <- mean(c(97.37,97.06,100,100,100,100,100,100,100,100))
VPP_MetaIntegrator <- mean(c(98.55,100,100,100,97.92,100,100,100,100))
VPP_massiR <- mean(c(98.48,98.91,100,100,100,100,100,100,100,100))
VPN_MetaIntegrator <- mean(c(97.37,100,97.83,100,99.15,95.24,100,100,100))
VPN_massiR <- mean(c(90.24,78.57,100,97.83,100,98.33,95.24,100,100,100))

datos <- data.frame(indices =  c('porc_aciertos','porc_aciertos',
                                'sensibilidad','sensibilidad',
                                'especificidad','especificidad',
                                'VPP','VPP',
                                'VPN','VPN'),
                  media_porcentajes = c(porc_aciertos_MetaIntegrator, porc_aciertos_massiR,
                              sensibilidad_MetaIntegrator, sensibilidad_massiR,
                              especificidad_MetaIntegrator, especificidad_massiR,
                              VPP_MetaIntegrator, VPP_massiR,
                              VPN_MetaIntegrator,VPN_massiR),
                  min=c(97.50, 92.54, 92.31, 91,97.37, 97.06, 97.92,98.48,95.24,78.58),
                  max=rep(100, times=10),
                  Método = rep(c("MetaIntegrator", "massiR"), times=5)
                    )


ggplot(data=datos,aes(x=indices, y=media_porcentajes,group=Método,col=Método))+
  ylab("Porcentaje (%)")+
  xlab("Índices de validación") +
  geom_errorbar(data=datos, aes(x=indices,ymin = min, ymax=  max,group=Método,col=Método), 
                width=0.2, size=0.3, alpha=1) +
  geom_point(aes(x=indices, y=media_porcentajes,group=Método,col=Método)) #+
  #geom_line()

# curvas ROC ----
library(pROC)
predictor <- imputedSex_GSE22255

obj <- roc(MetaAnalysis$originalData$GSE22255$pheno$`gender:ch1`,
           predictor=as.numeric(as.factor(predictor)))

plot.roc(obj,print.auc=T, grid=c(0.1, 0.1),
         col="blue",xlab="1-Especificidad",ylab="Sensibilidad",
         max.auc.polygon=TRUE, print.thres=TRUE)

# curvas PRC ----

library(caret)

y <- as.factor(MetaAnalysis$originalData$GSE22255$pheno$`gender:ch1`)
# factor of positive / negative cases

predictions <- as.factor(predictor)
# factor of predictions

precision <- posPredValue(predictions, y, positive="male")
recall <- sensitivity(predictions, y, positive="male")

F1 <- (2*precision*recall)/(precision + recall)

f1_score <- function(predicted, expected, positive.class="male") {
  predicted <- factor(as.character(predicted), levels=unique(as.character(expected)))
  expected  <- as.factor(expected)
  cm = as.matrix(table(expected, predicted))
  
  precision <- diag(cm) / colSums(cm)
  recall <- diag(cm) / rowSums(cm)
  f1 <-  ifelse(precision + recall == 0, 0, 2 * precision * recall / (precision + recall))
  
  #Assuming that F1 is zero when it's not possible compute it
  f1[is.na(f1)] <- 0
  
  #Binary F1 or Multi-class macro-averaged F1
  ifelse(nlevels(expected) == 2, f1[positive.class], mean(f1))
}

f1_score(predictor, y,"male")

###

install.packages("PRROC")
library(PRROC)

library(dplyr)
pr.curve(scores.class0=predictions, curve=TRUE, 
         weights.class0 = recode(y, "female"=1, "male"=0))
plot(pr.curve(scores.class0=predictions, curve=TRUE, 
              weights.class0 = recode(y, "female"=1, "male"=0)))

